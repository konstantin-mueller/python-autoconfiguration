import os
import shutil

from invoke import task


BLUE = "\033[94m"
GREEN = "\033[92m"
YELLOW = "\033[93m"
RED = "\033[91m"
RESET = "\033[0m"
BOLD = "\033[1m"

DIST_DIR = "dist"
BUILD_DIR = "build"
SRC_DIR = "autoconfiguration"
TESTS_DIR = "tests"


@task
def check_format(c):
    c.run("black --check .")


@task
def check_code(c):
    print(f"{BLUE}{BOLD}Running pylint:{RESET}\n")
    c.run(f"pylint --rcfile=.pylintrc {SRC_DIR}")

    print(f"\n{BLUE}{BOLD}Running mypy:{RESET}\n")
    c.run(f"mypy {SRC_DIR}")

    print(f"\n\n{BLUE}{BOLD}Running bandit:{RESET}\n")
    c.run(f"bandit -r {SRC_DIR}")


@task
def tests(c):
    c.run(f"coverage run --source {SRC_DIR} --branch -m pytest {TESTS_DIR}")
    c.run("coverage report -m --fail-under 100")


@task(pre=[check_format, check_code, tests])
def deploy(c):
    if os.path.exists(DIST_DIR):
        shutil.rmtree(DIST_DIR)

    if os.path.exists(BUILD_DIR):
        shutil.rmtree(BUILD_DIR)

    c.run("python setup.py sdist bdist_wheel")
    c.run("python -m twine upload dist/*")
