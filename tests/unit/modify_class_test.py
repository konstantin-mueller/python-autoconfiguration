from dataclasses import FrozenInstanceError

import pytest

from autoconfiguration.exceptions.config_class_errors import ConfigClassAttributeError
from autoconfiguration.helpers import modify_class
from autoconfiguration.helpers.modify_class import MISSING_VALUE


@pytest.fixture(autouse=True)
def mock_instances(mocker):
    mock = mocker.patch("autoconfiguration.helpers.modify_class.instances")
    mock.has.return_value = False
    return mock


def test_make_singleton_should_set_new_method():
    class Test:
        pass

    modify_class.make_singleton(Test)

    assert hasattr(Test, "__new__")


def test_make_singleton_new_should_set_instance(mock_instances):
    class Test:
        pass

    mock_instances.get_by.return_value = "test"

    modify_class.make_singleton(Test)
    Test()

    mock_instances.append.assert_called_once()
    mock_instances.get_by.assert_called_once_with(Test)


def test_make_singleton_new_should_return_initialized_instance(mock_instances):
    class Test:
        pass

    mock_instances.has.return_value = True

    modify_class.make_singleton(Test)
    Test()

    mock_instances.append.assert_not_called()
    mock_instances.get_by.assert_called_once_with(Test)


def test_add_contains_getattribute_getitem_get_methods_to_should_add_methods():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)

    assert hasattr(Test, "__contains__")
    assert hasattr(Test, "__getitem__")
    assert hasattr(Test, "get")
    assert hasattr(Test, "__getattribute__")


def test_add_contains_getattribute_getitem_get_methods_to_contains_should_check_if_item_exists():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)
    test = Test()
    test.abc = ""

    assert "abc" in test
    assert "a" not in test


def test_add_contains_getattribute_getitem_get_methods_to_getitem_should_return_value():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)
    test = Test()
    test.abc = 123

    assert test["abc"] == 123


def test_add_contains_getattribute_getitem_get_methods_to_getitem_when_missing_value_raises_error():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)
    test = Test()
    test.abc = MISSING_VALUE

    with pytest.raises(ConfigClassAttributeError):
        test["abc"]


def test_add_contains_getattribute_getitem_get_methods_to_get_should_return_value():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)
    test = Test()
    test.abc = 123

    assert test.get("abc") == 123
    assert test.get("a") is None
    assert test.get("a", 1) == 1


def test_add_contains_getattribute_getitem_get_methods_to_getattribute_should_return_value():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)
    test = Test()
    test.abc = 123

    assert test.abc == 123


def test_add_contains_getattribute_getitem_get_methods_to_getattribute_when_missing_value_should_raise_exception():
    class Test:
        pass

    modify_class.add_contains_getattribute_getitem_get_methods_to(Test)
    test = Test()
    test.abc = modify_class.MISSING_VALUE

    with pytest.raises(ConfigClassAttributeError):
        test.abc


def test_freeze_class_should_set_delattr_and_setattr_methods():
    class Test:
        pass

    modify_class.freeze_class(Test)

    assert hasattr(Test, "__delattr__")
    assert hasattr(Test, "__setattr__")


def test_freeze_class_when_delattr_and_setattr_called_should_raise_exception():
    class Test:
        pass

    modify_class.freeze_class(Test)

    with pytest.raises(FrozenInstanceError):
        del Test().a

    with pytest.raises(FrozenInstanceError):
        Test().a = 1
