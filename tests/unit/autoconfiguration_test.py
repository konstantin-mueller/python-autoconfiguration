import os
from dataclasses import dataclass

import pytest

from autoconfiguration import autoconfiguration
from autoconfiguration.exceptions.config_class_errors import (
    ConfigNotInitializedError,
    ConfigClassIsNotDataclassError,
)
from autoconfiguration.exceptions.config_dir_errors import ConfigDirNotFoundError
from autoconfiguration.exceptions.config_file_errors import NoConfigFilesFoundError
from autoconfiguration.helpers.modify_class import make_singleton


@dataclass
class Config:
    pass


make_singleton(Config)


@pytest.fixture(autouse=True)
def mock_create_config_instance(monkeypatch):
    return monkeypatch.setattr(
        autoconfiguration,
        autoconfiguration.create_config_instance.__name__,
        lambda cls, files: Config(),
    )


@pytest.fixture
def config():
    yield autoconfiguration
    autoconfiguration.reset()


@pytest.fixture(autouse=True)
def mock_instances(mocker):
    mock = mocker.patch("autoconfiguration.autoconfiguration.instances")
    mock.has.return_value = False
    return mock


def test_init_when_config_dir_does_not_exist_should_raise_error(monkeypatch, config):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: False)

    with pytest.raises(ConfigDirNotFoundError):
        config.init(config_class=Config)


def test_init_when_no_global_config_exists_should_raise_error(monkeypatch, config):
    monkeypatch.setattr(
        os.path,
        os.path.exists.__name__,
        lambda path: not path.endswith(autoconfiguration.GLOBAL_CONFIG),
    )

    with pytest.raises(NoConfigFilesFoundError):
        config.init(config_class=Config)


def test_init_when_config_initialized_should_return_initialized_instance(
    monkeypatch, config, mock_instances
):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    mock_instances.has.return_value = True

    instance = config.init(config_class=Config)
    second_instance = config.init(config_class=Config)
    assert instance is second_instance


def test_init_when_no_config_files_passed_should_only_load_global_config(
    monkeypatch, config, mocker
):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    mock_config = mocker.patch(
        "autoconfiguration.autoconfiguration.create_config_instance"
    )

    config_class = Config
    config.init(config_class=config_class)

    mock_config.assert_called_once_with(
        config_class, (f"config/{autoconfiguration.GLOBAL_CONFIG}",)
    )


def test_init_should_load_global_and_passed_configs(monkeypatch, config, mocker):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    mock_config = mocker.patch(
        "autoconfiguration.autoconfiguration.create_config_instance"
    )

    config_class = Config
    config.init("dev", "prod", config_class=config_class)

    mock_config.assert_called_once_with(
        config_class,
        (
            f"config/{config.GLOBAL_CONFIG}",
            f"config/{config.CONFIG_PREFIX}dev{config.CONFIG_EXTENSION}",
            f"config/{config.CONFIG_PREFIX}prod{config.CONFIG_EXTENSION}",
        ),
    )


def test_init_when_passed_config_contains_prefix_should_not_add_prefix(
    monkeypatch, config, mocker
):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    mock_config = mocker.patch(
        "autoconfiguration.autoconfiguration.create_config_instance"
    )

    config_class = Config
    config.init(f"{config.CONFIG_PREFIX}dev", config_class=config_class)

    mock_config.assert_called_once_with(
        config_class,
        (
            f"config/{config.GLOBAL_CONFIG}",
            f"config/{config.CONFIG_PREFIX}dev{config.CONFIG_EXTENSION}",
        ),
    )


def test_init_when_passed_config_contains_extension_should_not_add_extension(
    monkeypatch, config, mocker
):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    mock_config = mocker.patch(
        "autoconfiguration.autoconfiguration.create_config_instance"
    )

    config_class = Config
    config.init(
        f"{config.CONFIG_PREFIX}dev{config.CONFIG_EXTENSION}", config_class=config_class
    )

    mock_config.assert_called_once_with(
        config_class,
        (
            f"config/{config.GLOBAL_CONFIG}",
            f"config/{config.CONFIG_PREFIX}dev{config.CONFIG_EXTENSION}",
        ),
    )


def test_init_when_config_class_is_not_dataclass_should_raise_exception(config):
    class TestClass:
        pass

    with pytest.raises(ConfigClassIsNotDataclassError):
        config.init(config_class=TestClass)


def test_init_when_config_sub_class_is_not_dataclass_should_raise_exception(config):
    class SubClass:
        pass

    @dataclass
    class TestClass:
        sub: SubClass

    with pytest.raises(ConfigClassIsNotDataclassError):
        config.init(config_class=TestClass)


def test_init_when_a_config_file_could_not_be_found_should_log_warning(
    config, mocker, monkeypatch
):
    monkeypatch.setattr(
        os.path,
        os.path.exists.__name__,
        lambda path: not path.endswith("test.ini"),
    )
    mock_log = mocker.patch("autoconfiguration.autoconfiguration.LOG")

    config.init("test", config_class=Config)

    assert mock_log.warning.call_count == 1
    assert mock_log.warning.call_args[0][1][0] == "test"


def test_get_should_return_first_initialized_instance(
    monkeypatch, config, mock_instances
):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    instance = config.init(config_class=Config)
    mock_instances.get_first.return_value = instance

    assert instance is config.get()


def test_get_when_config_class_does_not_exists_should_raise_error(config):
    with pytest.raises(ConfigNotInitializedError):
        config.get(Config)


def test_get_should_return_initialized_instance(monkeypatch, config, mock_instances):
    monkeypatch.setattr(os.path, os.path.exists.__name__, lambda _: True)

    mock_instances.has.side_effect = False, True
    instance = config.init(config_class=Config)
    mock_instances.get_by.return_value = instance

    assert instance is config.get(Config)

    mock_instances.get_by.assert_called_once_with(Config)
