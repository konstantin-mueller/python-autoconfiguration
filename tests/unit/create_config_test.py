from dataclasses import dataclass, make_dataclass, field
from typing import Optional, List, Any, Tuple, Dict

import pytest

import autoconfiguration
from autoconfiguration.create_config import create_config_instance
from autoconfiguration.exceptions.data_type_errors import (
    UnableToConvertConfigValueToTypeError,
)
from autoconfiguration.helpers.modify_class import MISSING_VALUE


@dataclass
class Config:
    @dataclass
    class TestAbc:
        key_def: str
        key_optional: Optional[str]

    @dataclass
    class Abc:
        invalid: bool
        qwe: Optional[int]
        opt: Optional
        deg: str = "deg test"
        list: List = field(default_factory=lambda: [1])

    test_abc: TestAbc
    abc: Abc
    no_section: str
    optional_section: Optional[str]
    opt_section: Optional
    default_section: int = 123
    default_tuple: Tuple = field(default_factory=lambda: (2, 3))

    def __contains__(self, item):
        return item in self.__dict__


@pytest.fixture(autouse=True)
def mock_config_parser(mocker):
    config_data = {
        "test-abc": {"key-def": "value", "key_optional": "test"},
        "abc": {},
        "test": {
            "int": "123",
            "bool": "False",
            "float": "12.34",
            "complex": "1j",
            "list": '["abc", 123]',
            "tuple": '(123, "abc")',
            "dict": '{"test": 123, "second": "abc"}',
        },
    }

    mock = mocker.patch("autoconfiguration.create_config.ConfigParser")
    mock.return_value.sections.return_value = config_data.keys()
    mock.return_value.__getitem__ = lambda self, key: config_data[key]
    return mock


@pytest.fixture(autouse=True)
def mock_modify_class_helpers(monkeypatch):
    monkeypatch.setattr(
        autoconfiguration.create_config,
        autoconfiguration.create_config.make_singleton.__name__,
        lambda _: None,
    )
    monkeypatch.setattr(
        autoconfiguration.create_config,
        autoconfiguration.create_config.add_contains_getattribute_getitem_get_methods_to.__name__,
        lambda cls: None,
    )
    monkeypatch.setattr(
        autoconfiguration.create_config,
        autoconfiguration.create_config.freeze_class.__name__,
        lambda _: None,
    )


def test_create_config_instance_adds_all_sections_keys_and_values_of_config_to_config_with_valid_names():
    instance = create_config_instance(Config, ())

    assert "test_abc" in instance
    assert hasattr(instance.test_abc, "key_def")
    assert instance.test_abc.key_def == "value"

    assert "abc" in instance
    assert hasattr(instance.abc, "deg")
    assert hasattr(instance.abc, "qwe")


def test_create_config_instance_when_section_and_default_value_missing_and_not_optional_should_set_missing_value():
    instance = create_config_instance(Config, ())

    assert instance.no_section is MISSING_VALUE


def test_create_config_instance_when_section_value_missing_should_set_default_value():
    instance = create_config_instance(Config, ())

    assert instance.default_section == 123
    assert instance.default_tuple == (2, 3)


def test_create_config_instance_when_section_and_default_value_missing_should_set_none_for_optional():
    instance = create_config_instance(Config, ())

    assert instance.optional_section is None
    assert instance.opt_section is None


def test_create_config_instance_when_config_value_missing_should_set_default_value():
    instance = create_config_instance(Config, ())

    assert instance.abc.deg == "deg test"
    assert instance.abc.list == [1]


def test_create_config_instance_when_config_and_default_value_missing_should_set_none_for_optional():
    instance = create_config_instance(Config, ())

    assert instance.abc.qwe is None
    assert instance.abc.opt is None


def test_create_config_instance_when_config_and_default_value_missing_and_not_optional_should_set_missing_value():
    instance = create_config_instance(Config, ())

    assert instance.abc.invalid is MISSING_VALUE


def test_create_config_instance_should_convert_int():
    test = make_dataclass("Test", [("int", int)])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert instance.test.int == 123


def test_create_config_instance_should_convert_bool():
    test = make_dataclass("Test", [("bool", bool)])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert not instance.test.bool


def test_create_config_instance_should_convert_float():
    test = make_dataclass("Test", [("float", float)])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert instance.test.float == 12.34


def test_create_config_instance_should_convert_complex():
    test = make_dataclass("Test", [("complex", complex)])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert instance.test.complex == 1j


def test_create_config_instance_should_convert_list():
    test = make_dataclass("Test", [("list", List[Any])])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert instance.test.list == ["abc", 123]


def test_create_config_instance_should_convert_tuple():
    test = make_dataclass("Test", [("tuple", Tuple[int, str])])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert instance.test.tuple == (123, "abc")


def test_create_config_instance_should_convert_dict():
    test = make_dataclass("Test", [("dict", Dict)])
    cls = make_dataclass("Cls", [("test", test)])
    instance = create_config_instance(cls, ())

    assert instance.test.dict == {"test": 123, "second": "abc"}


def test_create_config_instance_when_value_has_wrong_type_should_raise_exception():
    test = make_dataclass("Test", [("int", float)])
    cls = make_dataclass("Cls", [("test", test)])

    with pytest.raises(UnableToConvertConfigValueToTypeError):
        create_config_instance(cls, ())


def test_create_config_instance_when_value_with_origin_type_has_wrong_type_should_raise_exception():
    test = make_dataclass("Test", [("tuple", List[str])])
    cls = make_dataclass("Cls", [("test", test)])

    with pytest.raises(UnableToConvertConfigValueToTypeError):
        create_config_instance(cls, ())
