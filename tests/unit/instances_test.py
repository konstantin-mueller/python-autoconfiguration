from autoconfiguration.exceptions.config_class_errors import ConfigNotInitializedError
import pytest

from autoconfiguration.helpers import instances


@pytest.fixture(autouse=True)
def reset_instances():
    yield
    instances.reset()


class Test:
    pass


@pytest.mark.parametrize("config_instances", [[], ["", (), 1]])
def test_reset_should_remove_cached_instances(config_instances):
    instances.Constants.config_instances = config_instances

    instances.reset()

    assert instances.Constants.config_instances == []


def test_append_should_add_instance():
    instances.append("test")
    instances.append(123)

    assert instances.Constants.config_instances == ["test", 123]


def test_get_by_should_return_instance_of_passed_class():
    instances.append(Test())
    result = instances.get_by(Test)

    assert isinstance(result, Test)


def test_get_by_when_instance_not_found_should_return_none():
    result = instances.get_by(str)

    assert result is None


@pytest.mark.parametrize("config_instances", [[], [1], [1, 2, 3]])
def test_has_any_should_check_if_any_instances_cached(config_instances):
    instances.Constants.config_instances = config_instances

    assert instances.has_any() == bool(config_instances)


def test_has_should_check_if_instance_of_passed_class_cached():
    instances.append(Test())

    assert instances.has(Test)


def test_has_when_instance_of_passed_class_not_cached_should_return_false():
    assert not instances.has(Test)


def test_get_first_should_return_first_cached_instance():
    instances.append("test")

    assert instances.get_first() == "test"


def test_get_first_when_no_instances_cached_should_raise_error():
    with pytest.raises(ConfigNotInitializedError):
        instances.get_first()
