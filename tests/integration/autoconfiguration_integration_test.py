import os
from dataclasses import dataclass, field
from typing import List, Tuple, Dict, Optional

import pytest

from autoconfiguration import autoconfiguration


@dataclass
class Config:
    @dataclass
    class TestSection:
        name_key: str
        test_key: str
        complex_value: str

    @dataclass
    class Types:
        int_v: int
        float_v: float
        complex_v: complex
        bool_v: bool
        list_v: List
        tuple_v: Tuple[float, complex, str]
        dict_v: Dict
        optional: Optional
        union_optional: Optional[str]
        dict: Dict = field(default_factory=lambda: {})

    @dataclass
    class Branch:
        env: str

    @dataclass
    class Abc_1:
        test_abc: str

    test_section: TestSection
    types: Types
    branch: Branch
    abc_1: Abc_1


@pytest.fixture
def config():
    yield autoconfiguration
    autoconfiguration.reset()


@pytest.fixture
def config_dir():
    return os.path.join(os.path.dirname(__file__), "config")


def test_init_should_load_global_config_by_default(config, config_dir):
    conf: Config = config.init(config_class=Config, config_dir=config_dir)

    assert "test_section" in conf
    assert conf.test_section.name_key == "key"
    assert conf.test_section.complex_value == "test %d 123 \\n\\ta\ntest second line"
    assert "test_key" not in conf.test_section
    assert "branch" not in conf


def test_init_should_convert_to_correct_data_types(config, config_dir):
    conf = config.init(config_class=Config, config_dir=config_dir)

    assert "types" in conf
    assert conf.types.int_v == 123
    assert conf.types.float_v == 6.034
    assert conf.types.complex_v == 1j
    assert conf.types.bool_v is False
    assert conf.types.list_v == [123, "abc", 0.12345]
    assert conf.types.tuple_v == (9.0863, 1j, "test")
    assert conf.types.dict_v == {1: "a", "b": 0.532, 1j: True, "list": [1, 2, "x"]}


def test_init_when_no_value_loaded_should_set_optional_to_none(config, config_dir):
    conf = config.init(config_class=Config, config_dir=config_dir)

    assert conf.types.optional is None
    assert conf.types.union_optional is None


def test_init_when_no_value_loaded_and_default_specified_should_set_to_default(
    config, config_dir
):
    conf = config.init(config_class=Config, config_dir=config_dir)

    assert conf.types.dict == {}


def test_init_should_load_passed_file(config, config_dir):
    conf = config.init("dev", config_class=Config, config_dir=config_dir)

    assert "test_section" in conf
    assert conf.test_section.name_key == "key-TEST"
    assert "test_key" not in conf.test_section
    assert "branch" in conf
    assert conf.branch.env == "dev"
    assert "abc_1" in conf
    assert conf.abc_1.test_abc == "test"


def test_init_should_load_multiple_passed_files_and_merge_them_in_order(
    config, config_dir
):
    conf = config.init("dev", "prod", config_class=Config, config_dir=config_dir)

    assert "test_section" in conf
    assert conf.test_section.name_key == "key-TEST"
    assert conf.test_section.test_key == "TEST"
    assert "branch" in conf
    assert conf.branch.env == "prod"
    assert "abc_1" in conf
    assert conf.abc_1.test_abc == "test"


def test_init_when_config_name_invalid_should_not_load_config(config, config_dir):
    conf = config.init("test", config_class=Config, config_dir=config_dir)

    # config should not contain sections of invalid file:
    assert "config_test" not in conf
    # but it should contain sections of the global file:
    assert "test_section" in conf
    assert conf.test_section.name_key == "key"


def test_get_when_config_instance_exists_should_return_instance(config, config_dir):
    conf = config.init("dev", config_class=Config, config_dir=config_dir)

    assert conf is config.get()
