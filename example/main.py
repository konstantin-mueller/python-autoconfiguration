"""
Run from root dir with:
python -m example.main
"""
import logging

import autoconfiguration

from example.config import Config, SecondConfig

logging.basicConfig(level=logging.DEBUG)
config: Config = autoconfiguration.init(
    # The order of the config files matters because the config files can depend on
    # each other. That means that the dev file does not need to contain all values of
    # the global file (config.ini). The config merges the global and dev files if
    # the dev profile is loaded. The global config file is always automatically loaded.
    "dev",
    config_class=Config,
    config_dir="example/config",
)
# Specifying the type (config: Config) enables auto completion in IDEs

print(config.branch)
print(config.credentials_secret.name)
print(config.types)
print()

# Config is a singleton. Another call to init with the same config class will not
# create a new instance. Instead it will return the already existing instance.
init: Config = autoconfiguration.init(
    "dev", "prod", config_class=Config, config_dir="example/config"
)
assert config is init
print(f"{config is init=}")
print(init.branch)
print(init.credentials_secret.name)
print()

# Use the get function to get an already initialized instance (the first initialized
# instance is always returned without a config class parameter):
get: Config = autoconfiguration.get()
assert config is get
print(f"{config is get=}")
print(config.branch)
print(config.credentials_secret.name)
# '-' in section and key names are replaced with '_':
print(config.abc_1)
print()

# Pass another config class to init to initialize another instance:
second: SecondConfig = autoconfiguration.init(
    "dev", "prod", config_class=SecondConfig, config_dir="example/config"
)
print(f"{config is second=}")
assert config is not second
print(second.branch)
print()

# A call to get without a config class parameter still returns the first instance:
get: Config = autoconfiguration.get()
assert second is not get
print(f"{second is get=}")
print()

# Pass the second config class to the get function to get the second instance:
second_get: SecondConfig = autoconfiguration.get(SecondConfig)
assert second is second_get
print(f"{second is second_get=}")
print()
