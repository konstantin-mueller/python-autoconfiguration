from dataclasses import dataclass, field
from typing import List, Tuple, Dict, Any, Optional


@dataclass
class Branch:
    env: str


@dataclass
class CredentialsSecret:
    name: str


@dataclass
class Abc_1:
    test_abc: List[int]


@dataclass
class Types:
    test_int: int
    test_bool: bool
    test_float: float
    test_complex: complex
    test_list: List[Any]
    test_tuple: Tuple[int, str]
    test_dict: Dict
    optional: Optional[str]
    default_int: int = 987
    default_list: List[str] = field(default_factory=lambda: [1, 2, 3])


@dataclass
class Config:
    branch: Branch
    credentials_secret: CredentialsSecret
    abc_1: Abc_1
    types: Types


@dataclass
class SecondConfig:
    branch: Branch
