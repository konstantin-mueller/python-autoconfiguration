"""Contains the base exception of the autoconfiguration package."""


class ConfigBaseError(Exception):
    """The base autoconfiguration exception."""
